#include "MainWindow.hpp"

#include <QApplication>

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  accacult_demo::accMainWindow w;
  w.show();
  return QApplication::exec();
}
