#ifndef TPMAINWINDOW_HPP
#define TPMAINWINDOW_HPP
#include <QMainWindow>

namespace accacult_demo {
class accMainWindow : public QMainWindow {
  Q_OBJECT

public:
  accMainWindow(QWidget *parent = nullptr);
};
} // namespace accacult_demo
#endif // TPMAINWINDOW_HPP
