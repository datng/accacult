#include "Widget_NameGenerator.hpp"

#include <QCheckBox>
#include <QDebug>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>

namespace accacult_demo {
namespace helper::widget_NameGenerator {
void ConnectSourceToGenerator(QLineEdit *source, std::vector<std::string> &nameList) {
  QObject::connect(source, &QLineEdit::editingFinished, [source, &nameList] {
    QStringList list = source->text().simplified().split(",");
    auto personalNames = std::vector<std::string>();
    for (const auto &entry : list) {
      personalNames.emplace_back(entry.simplified().toStdString());
    }
    nameList = personalNames;
  });
  emit source->editingFinished(); // to assign the default values
}

const static QString enabledStringNo = "Enabled: No";
const static QString enabledStringRandom = "Enabled: Random";
const static QString enabledStringYes = "Enabled: Yes";

void ConnectButtonToToggle(QPushButton *button, accacult::NameGenerator::Toggle &toggle) {
  switch (toggle) {
  case accacult::NameGenerator::Toggle::No:
    button->setText(enabledStringNo);
    break;
  case accacult::NameGenerator::Toggle::Random:
    button->setText(enabledStringRandom);
    break;
  case accacult::NameGenerator::Toggle::Yes:
    button->setText(enabledStringYes);
    break;
  };

  QObject::connect(button, &QPushButton::clicked, [button, &toggle] {
    switch (toggle) {
    case accacult::NameGenerator::Toggle::No:
      toggle = accacult::NameGenerator::Toggle::Random;
      button->setText(enabledStringRandom);
      break;
    case accacult::NameGenerator::Toggle::Random:
      toggle = accacult::NameGenerator::Toggle::Yes;
      button->setText(enabledStringYes);
      break;
    case accacult::NameGenerator::Toggle::Yes:
      toggle = accacult::NameGenerator::Toggle::No;
      button->setText(enabledStringNo);
      break;
    }
  });
}
} // namespace helper::widget_NameGenerator

accWidget_NameGenerator::accWidget_NameGenerator(QWidget *parent)
    : QWidget(parent), mGenerator(std::make_unique<accacult::NameGenerator>()) {
  using helper::widget_NameGenerator::ConnectButtonToToggle;
  using helper::widget_NameGenerator::ConnectSourceToGenerator;

  auto layout = new QVBoxLayout(this);
  auto buttonGenerate = new QPushButton("Generate new name", this);
  auto generatedName = new QLabel("Generated name:", this);

  connect(buttonGenerate, &QPushButton::clicked, this, [this, generatedName]() {
    auto name = mGenerator->Next();
    generatedName->setText(         //
        QString("Generated name: ") //
        + name.Personal.c_str()     //
        + (name.Family.has_value() ? (QString(" ") + name.Family->c_str()) : ""));
  });
  auto settings = new QGroupBox("Settings", this);
  auto settingsLayout = new QGridLayout(settings);

  // personal names
  {
    auto label = new QLabel("Personal names: ", settings);
    auto lineEdit = new QLineEdit("Eeny, Meeny, Miny, Moe", settings);
    ConnectSourceToGenerator(lineEdit, mGenerator->Personals);
    settingsLayout->addWidget(label, 0, 0);
    settingsLayout->addWidget(lineEdit, 0, 2);
  }

  // courtesy names
  {
    auto label = new QLabel("Courtesy names: ", settings);
    auto button = new QPushButton(settings);
    ConnectButtonToToggle(button, mGenerator->WithCourtesy);
    auto lineEdit = new QLineEdit(settings);
    ConnectSourceToGenerator(lineEdit, mGenerator->Courtesies);
    settingsLayout->addWidget(label, 1, 0);
    settingsLayout->addWidget(button, 1, 1);
    settingsLayout->addWidget(lineEdit, 1, 2);
  }

  // family names
  {
    auto label = new QLabel("Family names: ", settings);
    auto button = new QPushButton(settings);
    ConnectButtonToToggle(button, mGenerator->WithFamily);
    auto lineEdit = new QLineEdit("Skidum, Skidee, Skidoo", settings);
    ConnectSourceToGenerator(lineEdit, mGenerator->Families);
    settingsLayout->addWidget(label, 2, 0);
    settingsLayout->addWidget(button, 2, 1);
    settingsLayout->addWidget(lineEdit, 2, 2);
  }

  // matronymics
  {
    auto label = new QLabel("Matronymics: ", settings);
    auto button = new QPushButton(settings);
    ConnectButtonToToggle(button, mGenerator->WithMatronymic);
    auto lineEdit = new QLineEdit(settings);
    ConnectSourceToGenerator(lineEdit, mGenerator->Matronymics);
    settingsLayout->addWidget(label, 3, 0);
    settingsLayout->addWidget(button, 3, 1);
    settingsLayout->addWidget(lineEdit, 3, 2);
  }

  // patronymics
  {
    auto label = new QLabel("Patronymics: ", settings);
    auto button = new QPushButton(settings);
    ConnectButtonToToggle(button, mGenerator->WithPatronymic);
    auto lineEdit = new QLineEdit(settings);
    ConnectSourceToGenerator(lineEdit, mGenerator->Patronymics);
    settingsLayout->addWidget(label, 4, 0);
    settingsLayout->addWidget(button, 4, 1);
    settingsLayout->addWidget(lineEdit, 4, 2);
  }

  // prefixes
  {
    auto label = new QLabel("Prefixes: ", settings);
    auto button = new QPushButton(settings);
    ConnectButtonToToggle(button, mGenerator->WithPrefix);
    auto lineEdit = new QLineEdit(settings);
    ConnectSourceToGenerator(lineEdit, mGenerator->Prefixes);
    settingsLayout->addWidget(label, 5, 0);
    settingsLayout->addWidget(button, 5, 1);
    settingsLayout->addWidget(lineEdit, 5, 2);
  }

  // suffixes
  {
    auto label = new QLabel("Suffixes: ", settings);
    auto button = new QPushButton(settings);
    ConnectButtonToToggle(button, mGenerator->WithSuffix);
    auto lineEdit = new QLineEdit(settings);
    ConnectSourceToGenerator(lineEdit, mGenerator->Suffixes);
    settingsLayout->addWidget(label, 6, 0);
    settingsLayout->addWidget(button, 6, 1);
    settingsLayout->addWidget(lineEdit, 6, 2);
  }

  settings->setLayout(settingsLayout);

  auto spacer = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding);

  layout->addWidget(generatedName);
  layout->addWidget(buttonGenerate);
  layout->addWidget(settings);
  layout->addItem(spacer);
  setLayout(layout);
}
} // namespace accacult_demo
