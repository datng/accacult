#include "MainWindow.hpp"

#include "Widget_NameGenerator.hpp"

#include <QTabWidget>
#include <QToolButton>

namespace accacult_demo {
accMainWindow::accMainWindow(QWidget *parent) : QMainWindow(parent) {
  setGeometry(0, 0, 800, 600);
  auto tabWidget = new QTabWidget(this);
  tabWidget->setTabPosition(QTabWidget::West);
  auto widget_NameGenerator = new accWidget_NameGenerator(this);
  tabWidget->addTab(widget_NameGenerator, "Name Generator");
  setCentralWidget(tabWidget);
  tabWidget->setGeometry(this->x(), this->y(), this->width(), this->height());
}
} // namespace accacult_demo
