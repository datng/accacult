#pragma once
#include <QWidget>
#include <accacult/NameGenerator.hpp>

namespace accacult_demo {
class accWidget_NameGenerator : public QWidget {
  Q_OBJECT
public:
  accWidget_NameGenerator(QWidget *parent);

private:
  std::unique_ptr<accacult::NameGenerator> mGenerator;
};
} // namespace accacult_demo
