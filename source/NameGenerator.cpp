#include <accacult/NameGenerator.hpp>

#include <chrono>
#include <iostream>
#include <random>
#include <set>

namespace accacult {
struct NameGenerator::Details {
  Details()
      : mDistribution(new std::uniform_int_distribution<size_t>(0, std::numeric_limits<size_t>::max())),
        mGenerator(static_cast<int>(std::chrono::steady_clock::now().time_since_epoch().count())) {}
  Details(const Details &) = delete;
  Details(Details &&) = delete;

  ~Details() { delete mDistribution; }

  auto operator=(const Details &) -> Details & = delete;
  auto operator=(Details &&) -> Details & = delete;

  auto NextRandomNumber() -> size_t { return mDistribution->operator()(mGenerator); }

private:
  std::uniform_int_distribution<size_t> *mDistribution;
  std::default_random_engine mGenerator;
};

NameGenerator::NameGenerator() : mDetails(new Details) {}

NameGenerator::~NameGenerator() { delete mDetails; }

NameGenerator::NameGenerator(const NameGenerator &other) : NameGenerator() {
  Courtesies = other.Courtesies;
  Families = other.Families;
  Matronymics = other.Matronymics;
  Middles = other.Middles;
  Patronymics = other.Patronymics;
  Personals = other.Personals;
  Prefixes = other.Prefixes;
  Suffixes = other.Suffixes;
  Titles = other.Titles;

  WithCourtesy = other.WithCourtesy;
  WithFamily = other.WithFamily;
  WithMatronymic = other.WithMatronymic;
  WithPatronymic = other.WithPatronymic;
  WithPrefix = other.WithPrefix;
  WithSuffix = other.WithSuffix;
  WithTitle = other.WithTitle;

  mMaxNumberOfMiddles = other.mMaxNumberOfMiddles;
  mMinNumberOfMiddles = other.mMinNumberOfMiddles;
}

NameGenerator::NameGenerator(NameGenerator &&other) noexcept : NameGenerator() {
  Courtesies = std::move(other.Courtesies);
  Families = std::move(other.Families);
  Matronymics = std::move(other.Matronymics);
  Middles = std::move(other.Middles);
  Patronymics = std::move(other.Patronymics);
  Personals = std::move(other.Personals);
  Prefixes = std::move(other.Prefixes);
  Suffixes = std::move(other.Suffixes);
  Titles = std::move(other.Titles);

  WithCourtesy = other.WithCourtesy;
  WithFamily = other.WithFamily;
  WithMatronymic = other.WithMatronymic;
  WithPatronymic = other.WithPatronymic;
  WithPrefix = other.WithPrefix;
  WithSuffix = other.WithSuffix;
  WithTitle = other.WithTitle;

  mMaxNumberOfMiddles = other.mMaxNumberOfMiddles;
  mMinNumberOfMiddles = other.mMinNumberOfMiddles;
}

auto NameGenerator::operator=(const NameGenerator &other) -> NameGenerator & {
  // cargo culting
  NameGenerator tmp(other);
  std::swap(*this, tmp);
  return *this;
}

auto NameGenerator::operator=(NameGenerator &&other) noexcept -> NameGenerator & {
  // cargo culting
  NameGenerator tmp(std::move(other));
  std::swap(*this, other);
  return *this;
}

auto NameGenerator::MaxNumberOfMiddles() const -> uint8_t { return mMaxNumberOfMiddles; }

auto NameGenerator::MinNumberOfMiddles() const -> uint8_t { return mMinNumberOfMiddles; }

auto NameGenerator::Next() -> Name {
  Name name;
  if (Personals.empty()) {
    std::cerr << __FUNCTION__ << ": Personal name list is empty." << std::endl;
  } else {
    name.Personal = Personals.at(mDetails->NextRandomNumber() % Personals.size());
  }

  if (BoolFromToggleAndRandom(WithCourtesy)) {
    if (Courtesies.empty()) {
      std::cerr << __FUNCTION__ << ": Courtesy name list is empty." << std::endl;
    } else {
      name.Courtesy = Courtesies.at(mDetails->NextRandomNumber() % Courtesies.size());
    }
  }

  if (BoolFromToggleAndRandom(WithFamily)) {
    if (Families.empty()) {
      std::cerr << __FUNCTION__ << ": Family name list is empty." << std::endl;
    } else {
      name.Family = Families.at(mDetails->NextRandomNumber() % Families.size());
    }
  }

  if (BoolFromToggleAndRandom(WithMatronymic)) {
    if (Matronymics.empty()) {
      std::cerr << __FUNCTION__ << ": Matronymic name list is empty." << std::endl;
    } else {
      name.Matronymic = Matronymics.at(mDetails->NextRandomNumber() % Matronymics.size());
    }
  }

  if (mMaxNumberOfMiddles > Middles.size() //
      || mMinNumberOfMiddles > Middles.size()) {
    std::cerr << __FUNCTION__ << ": Adjusting middle name limits..." << std::endl;
    SetMaxNumberOfMiddles(static_cast<uint8_t>(Middles.size()));
  }

  if (mMaxNumberOfMiddles > 0) {
    auto diff = mMaxNumberOfMiddles - mMinNumberOfMiddles;
    uint8_t numberOfMiddles = 0;
    if (diff > 0) {
      numberOfMiddles = uint8_t(mDetails->NextRandomNumber()) % diff;
    } else {
      numberOfMiddles = mMinNumberOfMiddles;
    }
    std::set<std::string> middleNames;
    while (middleNames.size() < numberOfMiddles) {
      auto tempMiddle = Middles.at(mDetails->NextRandomNumber() % Middles.size());
      if (!middleNames.contains(tempMiddle)) {
        middleNames.insert(tempMiddle);
        name.Middles.emplace_back(tempMiddle);
      }
    }
  }

  if (BoolFromToggleAndRandom(WithPatronymic)) {
    if (Patronymics.empty()) {
      std::cerr << __FUNCTION__ << ": Patronymic name list is empty." << std::endl;
    } else {
      name.Patronymic = Patronymics.at(mDetails->NextRandomNumber() % Patronymics.size());
    }
  }

  if (BoolFromToggleAndRandom(WithPrefix)) {
    if (Prefixes.empty()) {
      std::cerr << __FUNCTION__ << ": Prefix list is empty." << std::endl;
    } else {
      name.Prefix = Prefixes.at(mDetails->NextRandomNumber() % Prefixes.size());
    }
  }

  if (BoolFromToggleAndRandom(WithSuffix)) {
    if (Suffixes.empty()) {
      std::cerr << __FUNCTION__ << ": Suffix list is empty." << std::endl;
    } else {
      name.Suffix = Suffixes.at(mDetails->NextRandomNumber() % Suffixes.size());
    }
  }

  if (BoolFromToggleAndRandom(WithTitle)) {
    if (Titles.empty()) {
      std::cerr << __FUNCTION__ << ": Title list is empty." << std::endl;
    } else {
      name.Title = Titles.at(mDetails->NextRandomNumber() % Titles.size());
    }
  }

  return name;
}

void NameGenerator::SetMaxNumberOfMiddles(const uint8_t &value) {
  if (value < mMinNumberOfMiddles) {
    mMinNumberOfMiddles = value;
  }
  mMaxNumberOfMiddles = value;
}

void NameGenerator::SetMinNumberOfMiddles(const uint8_t &value) {
  if (value > mMaxNumberOfMiddles) {
    mMaxNumberOfMiddles = value;
  }
  mMinNumberOfMiddles = value;
}

auto NameGenerator::BoolFromToggleAndRandom(const Toggle &toggle) -> bool {
  switch (toggle) {
  case Toggle::No:
    return false;
  case Toggle::Random:
    return static_cast<bool>(mDetails->NextRandomNumber() % 2);
  case Toggle::Yes:
    return true;
  }
}

} // namespace accacult
