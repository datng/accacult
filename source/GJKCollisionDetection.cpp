#include <accacult/GJKCollisionDetection.hpp>

namespace accacult::gjk {
float dotProduct(vec3 v1, vec3 v2) { return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z; }

size_t findExtreme(vec3 direction, std::vector<vec3> points) {
  size_t extreme = 0;
  for (size_t i = 1; i < points.size(); ++i) {
    if (dotProduct(direction, points[extreme]) > dotProduct(direction, points[i])) {
      extreme = i;
    }
  }

  return extreme;
}
} // namespace accacult::gjk
