#include <gtest/gtest.h>

#include <accacult/NumberSeries/FibbonacciAt.tpp>
#include <accacult/NumberSeries/NumberSeries.hpp>

using namespace accacult::number_series;

TEST(FibbonacciAt, SmokeTest) {
  EXPECT_EQ(FibbonacciAt<0>(), 0);
  EXPECT_EQ(FibbonacciAt<1>(), 1);
  EXPECT_EQ(FibbonacciAt<10>(), 55);
  EXPECT_EQ(FibbonacciAt<20>(), 6765);
}
