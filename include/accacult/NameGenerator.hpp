#ifndef RANDOMNAMEGENERATOR_HPP
#define RANDOMNAMEGENERATOR_HPP
#include <optional>
#include <string>
#include <vector>

namespace accacult {
struct Name {
  std::optional<std::string> Courtesy;
  std::optional<std::string> Family;
  std::optional<std::string> Matronymic;
  std::vector<std::string> Middles;
  std::optional<std::string> Patronymic;
  std::string Personal;
  std::optional<std::string> Prefix;
  std::optional<std::string> Suffix;
  std::optional<std::string> Title;
};

class NameGenerator {
public:
  enum class Toggle { No, Random, Yes };

  NameGenerator();
  ~NameGenerator();
  NameGenerator(const NameGenerator &);
  NameGenerator(NameGenerator &&) noexcept;
  NameGenerator &operator=(const NameGenerator &);
  NameGenerator &operator=(NameGenerator &&) noexcept;
  uint8_t MaxNumberOfMiddles() const;
  uint8_t MinNumberOfMiddles() const;
  void SetMaxNumberOfMiddles(const uint8_t &);
  void SetMinNumberOfMiddles(const uint8_t &);

  Name Next(); //!< Generate and return a new name.

  std::vector<std::string> Courtesies;
  std::vector<std::string> Families;
  std::vector<std::string> Matronymics;
  std::vector<std::string> Middles;
  std::vector<std::string> Patronymics;
  std::vector<std::string> Personals;
  std::vector<std::string> Prefixes;
  std::vector<std::string> Suffixes;
  std::vector<std::string> Titles;

  Toggle WithCourtesy = Toggle::Random;
  Toggle WithFamily = Toggle::Random;
  Toggle WithMatronymic = Toggle::Random;
  Toggle WithPatronymic = Toggle::Random;
  Toggle WithPrefix = Toggle::Random;
  Toggle WithSuffix = Toggle::Random;
  Toggle WithTitle = Toggle::Random;

private:
  struct Details; // details on the random number generator
  Details *mDetails;

  uint8_t mMaxNumberOfMiddles{};
  uint8_t mMinNumberOfMiddles{};
  bool BoolFromToggleAndRandom(const Toggle &toggle);
};
} // namespace accacult

#endif
