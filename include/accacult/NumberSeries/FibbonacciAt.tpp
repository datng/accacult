#include "NumberSeries.hpp"

namespace accacult::number_series {
template <size_t nth> constexpr size_t FibbonacciAt() { return FibbonacciAt<nth - 1>() + FibbonacciAt<nth - 2>(); }
template <> constexpr size_t FibbonacciAt<0>() { return 0; }
template <> constexpr size_t FibbonacciAt<1>() { return 1; }
} // namespace accacult::number_series
