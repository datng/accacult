#ifndef ACCACULT_NUMBERSERIES_HPP
#define ACCACULT_NUMBERSERIES_HPP
#include <cstddef>

namespace accacult::number_series {
template <size_t nth> constexpr size_t FibbonacciAt();
}
#endif // ACCACULT_NUMBERSERIES_HPP
