#ifndef GJKCOLLISIONDETECTION_HPP
#define GJKCOLLISIONDETECTION_HPP

#include <vector>

namespace accacult {
namespace gjk {
class vec3 {
public:
  union {
    float x;
    float r;
  };

  union {
    float y;
    float g;
  };

  union {
    float z;
    float b;
  };
};

float dotProduct(vec3 v1, vec3 v2);

/**
 * @brief findExtreme finds the furthest point away from a direction.
 * For example, if the direction is positive x axis, then it finds the point with the lowest x value.
 * @param direction
 * @param points the list of points in 3D space.
 * @return the index of the extreme point.
 */
size_t findExtreme(vec3 direction, std::vector<vec3> points);
} // namespace gjk
} // namespace accacult
#endif // GJKCOLLISIONDETECTION_HPP
